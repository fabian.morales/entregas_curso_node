let listaAlumnos = require('./datos/alumnos.json');
const fs = require('fs');

let alumnoToString = alumno => {
    let { cedula, nombre, promedio } = alumno;
    return 'Nombre: ' + nombre + ', cédula: ' + cedula + ((!isNaN(promedio)) ? ', promedio: ' + promedio : '');
}

let escribirArchivo = (callback) => {
    fs.writeFile('./datos/alumnos.json', JSON.stringify(listaAlumnos), (err) => {
        if (err){
            throw (err);
        }
        else{
            callback();
        }
    });
}

let agregarAlumno = (alumno, callback) => {
    if (listaAlumnos == null) {
        listaAlumnos = [];
    }

    if (alumno.nombre === '') {
        throw('Debe ingresar el nombre del alumno');
    }

    if (alumno.cedula === '') {
        throw('Debe ingresar la cédula del alumno');
    }

    if (alumno.telefono === '') {
        throw('Debe ingresar el teléfono del alumno');
    }

    if (alumno.correo === '') {
        throw('Debe ingresar el correo del alumno');
    }

    if (buscarAlumno(alumno.cedula) !== null){
        throw('¡El alumno ya está registrado!');
    }

    alumno.promedio = 0;

    listaAlumnos.push(alumno);
    escribirArchivo(() => {
        if (callback !== null && typeof callback !== 'undefined') {
            callback();
        }
    });
}

let mostrarAlumno = alumno => {
    console.log(alumnoToString(alumno));
}

let buscarAlumno = cedula => {
    let alumno = listaAlumnos.find(e => {
        return e.cedula === cedula;
    });

    if (typeof alumno === 'undefined') {
        alumno = null;
    }

    return alumno;
}

let actualizarAlumno = (alumno, callback) => {
    if (alumno.nombre === '') {
        throw('Debe ingresar el nombre del alumno');
    }

    if (alumno.cedula === '') {
        throw('Debe ingresar la cédula del alumno');
    }

    if (alumno.telefono === '') {
        throw('Debe ingresar el teléfono del alumno');
    }

    if (alumno.correo === '') {
        throw('Debe ingresar el correo del alumno');
    }

    let alumnoTmp = buscarAlumno(alumno.cedula);
    if (alumnoTmp === null) {
        throw('Alumno no encontrado!');
    }

    alumnoTmp.nombre = alumno.nombre;
    alumnoTmp.telefono = alumno.telefono;
    alumnoTmp.correo = alumno.correo;

    let index = listaAlumnos.indexOf(alumnoTmp);
    listaAlumnos[index] = alumnoTmp;

    escribirArchivo(() => {
        if (callback !== null && typeof callback !== 'undefined') {
            callback();
        }
    });
}

let obtenerListaAlumnos = () => {
    return listaAlumnos;
}

let mostrarListaAlumnos = () => {
    listaAlumnos.forEach((alumno) => {
        mostrarAlumno(alumno);
    });
}

let agregarInscripcion = (cedula, curso, callback) => {
    let alumno = buscarAlumno(cedula);
    if (alumno === null) {
        throw('¡Alumno no encontrado!');
    }

    let index = listaAlumnos.indexOf(alumno);

    if (alumno.inscripciones !== null && typeof alumno.inscripciones !== 'undefined') {
        let inscripcion = alumno.inscripciones.find(e => {
            return e.id === curso.id;
        });
        
        if (inscripcion !== null && typeof inscripcion !== 'undefined') {
            throw('¡El alumno ya se encuentra inscrito en este curso!')
        }
    }
    else{
        alumno.inscripciones = [];
    }

    alumno.inscripciones.push(curso);
    alumno.promedio = calcularPromedio(alumno);
    listaAlumnos[index] = alumno;

    escribirArchivo(() => {
        if (callback !== null && typeof callback !== 'undefined') {
            callback();
        }
    });
}

let quitarInscripcion = (cedula, idCurso, callback) => {
    let alumno = buscarAlumno(cedula);
    if (alumno === null) {
        throw('¡Alumno no encontrado!');
    }

    let index = listaAlumnos.indexOf(alumno);

    if (alumno.inscripciones === null || typeof alumno.inscripciones === 'undefined') {
        throw('¡El alumno no está inscrito en ningún curso!');
    }

    let inscripciones = alumno.inscripciones.filter(e => {
        return e.id !== idCurso;
    });

    if (inscripciones.length === alumno.inscripciones.length) {
        throw('¡El alumno no está inscrito en este curso!');
    }

    alumno.inscripciones = inscripciones;
    alumno.promedio = calcularPromedio(alumno);
    listaAlumnos[index] = alumno;

    escribirArchivo(() => {
        if (callback !== null && typeof callback !== 'undefined') {
            callback();
        }
    });
}

let asignarNota = (cedula, idCurso, nota, callback) => {
    let alumno = buscarAlumno(cedula);
    if (alumno === null) {
        throw('¡Alumno no encontrado!');
    }
    
    let index = listaAlumnos.indexOf(alumno);

    if (alumno.inscripciones !== null && typeof alumno.inscripciones !== 'undefined') {
        let inscripcion = alumno.inscripciones.find(e => {
            return e.id === idCurso;
        });

        if (inscripcion === null) {
            throw('¡El alumno no tiene inscrito este curso!')
        }

        let indexInscr = alumno.inscripciones.indexOf(inscripcion);
        inscripcion.nota = parseFloat(nota);
        alumno.inscripciones[indexInscr] = inscripcion;
        alumno.promedio = calcularPromedio(alumno);
        listaAlumnos[index] = alumno;

        escribirArchivo(() => {
            if (callback !== null && typeof callback !== 'undefined') {
                callback();
            }
        });
    }
    else{
        throw('¡El alumno no tiene cursos inscritos!');
    }
}

let mostrarInscripciones = cedula => {
    let alumno = buscarAlumno(cedula);
    if (alumno === null) {
        throw('¡Alumno no encontrado!');
    }

    if (alumno.inscripciones === null || typeof alumno.inscripciones === 'undefined' || alumno.inscripciones.length === 0) {
        throw('¡El alumno no tiene cursos inscritos!')
    }

    console.log('El alumno ' + alumno.cedula + '-' + alumno.nombre + ' tiene inscritos los siguientes cursos: \n');

    alumno.inscripciones.forEach(e => {
        console.log('Curso ID#' + e.id + ' - ' + e.nombre + '. Nota: ' + (!isNaN(e.nota) ? parseFloat(e.nota) : 0));
    });
}

let calcularPromedio = alumno => {
    let promedio = 0;
    let suma = 0;
    if (alumno.inscripciones !== null && typeof alumno.inscripciones !== 'undefined' && alumno.inscripciones.length > 0) {
        alumno.inscripciones.forEach(e => {
            suma += (!isNaN(e.nota) ? parseFloat(e.nota) : 0);
        });

        promedio = suma / alumno.inscripciones.length;
    }

    return promedio;
}

let mostrarPromediosAltos = () => {
    console.log('Estos son los alumnos con promedios mayores a 4.0: \n');

    let alumnos = listaAlumnos.filter(e => {
        return (!isNaN(e.promedio) ? e.promedio : 0) > 4;
    });

    if (alumnos.length === 0) {
        throw('¡No hay alumnos con promedio alto para mostrar!')
    }

    alumnos.forEach(e => {
        console.log('Alumno: ' + e.nombre + '. Promedio de notas: ' + Math.round(e.promedio * 100) / 100);
    });
}

module.exports = {
    alumnoToString,
    mostrarAlumno,
    buscarAlumno,
    mostrarListaAlumnos,
    obtenerListaAlumnos,
    agregarAlumno,
    actualizarAlumno,
    agregarInscripcion,
    quitarInscripcion,
    asignarNota,
    mostrarInscripciones,
    calcularPromedio,
    mostrarPromediosAltos
};
