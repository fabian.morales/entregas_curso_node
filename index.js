const cursos = require('./curso');
const alumnos = require('./alumno');
const { iniciarRegistro } = require('./registro');
const flash = require('./flash');

const express = require('express');
const methodOverride = require('method-override');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const path = require('path');

var app = express();
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        return req.body._method;
    }
}));

const dirModules = path.join(__dirname, '/node_modules');
console.log(dirModules);

app.use('/css', express.static(dirModules + '/bootstrap/dist/css'));
app.use('/js', express.static(dirModules + '/bootstrap/js/dist'));
app.use('/js', express.static(dirModules + '/jquery/dist'));
app.use('/js', express.static(dirModules + '/popper.js/dist/umd'));

app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials/');
hbs.registerHelper('comparar', (valor1, valor2, options) => {
    if (valor1 === valor2) {
        return options.fn(this);
    }
});

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/cursos', (req, res) => {
    let { mensajes, errores} = flash.obtenerFlash();
    res.render('cursos/lista', {'cursos' : cursos.obtenerListaCursos(), mensajes : mensajes, errores: errores});
});

app.get('/cursos/disponibles', (req, res) => {
    let { mensajes, errores} = flash.obtenerFlash();
    res.render('cursos/disponibles', {'cursos' : cursos.obtenerListaCursos(true), mensajes : mensajes, errores: errores});
});

app.get('/cursos/disponibles/:id', (req, res) => {
    try {
        let { mensajes, errores} = flash.obtenerFlash();
        let curso = cursos.buscarCurso(parseInt(req.params.id));

        if (curso === null || typeof curso === 'undefined') {
            throw('El curso no existe');
        }

        if (curso.estado !== 'disponible') {
            throw('El curso está cerrado');
        }

        res.render('cursos/detalle', {'curso' : curso, mensajes : mensajes, errores: errores});  
    } catch (error) {
        flash.agregarError(error);
        res.redirect('/cursos/disponibles');
    }
});

app.get('/cursos/crear', (req, res) => {
    res.render('cursos/crear');
});

app.get('/cursos/:id', (req, res) => {
    let { mensajes, errores} = flash.obtenerFlash();
    let curso = cursos.buscarCurso(parseInt(req.params.id));
    res.render('cursos/editar', {'curso' : curso, mensajes : mensajes, errores: errores});
});

app.post('/cursos', (req, res) => {
    try {
        let cursoNuevo = {
            id: parseInt(req.body.id),
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            intensidad: parseInt(req.body.intensidad),
            valor: parseInt(req.body.valor),
            modalidad: req.body.modalidad,
            estado: req.body.estado
        };
        cursos.agregarCurso(cursoNuevo);
        flash.agregarMensaje('Curso creado exitosamente');
    } catch (error) {
        flash.agregarError(error);
    }

    res.redirect('/cursos');
});

app.put('/cursos/:id', (req, res) => {
    try {
        let curso = cursos.buscarCurso(parseInt(req.params.id));
        if (curso !== null){
            curso.nombre = req.body.nombre;
            curso.intensidad = parseInt(req.body.intensidad);
            curso.descripcion = req.body.descripcion;
            curso.valor = parseInt(req.body.valor);
            curso.modalidad = req.body.modalidad;
            curso.estado = req.body.estado;
            cursos.actualizarCurso(curso);
        }
        flash.agregarMensaje('Curso guardado exitosamente');
    } catch (error) {
        flash.agregarMensaje(error);
    }
    res.redirect('/cursos');
});

app.get('/cursos/:id/inscripciones', (req, res) => {
    try {
        let curso = cursos.buscarCurso(parseInt(req.params.id));

        if (curso === null) {
            throw('¡Curso no encontrado!');
        }

        let listaAlumnos = alumnos.obtenerListaAlumnos();
    
        let inscritos = listaAlumnos.filter(a => {
            let c = a.inscripciones.filter(e => {
                return e.id === parseInt(curso.id);
            });
    
            return c !== null && typeof c !== 'undefined' && c.length > 0;
        });
    
        let { mensajes, errores} = flash.obtenerFlash();
        res.render('cursos/inscritos', {'alumnos' : inscritos, 'curso' : curso, mensajes : mensajes, errores: errores});
    } catch (error) {
        flash.agregarError(error);
        res.redirect('/cursos');
    }
});

app.get('/cursos/:id/cerrar', (req, res) => {
    try {
        let curso = cursos.buscarCurso(parseInt(req.params.id));
        if (curso === null) {
            throw('¡Curso no encontrado!');
        }

        curso.estado = 'cerrado';
        flash.agregarMensaje('Curso cerrado exitosamente');
    } catch (error) {
        flash.agregarError(error);
    }
    res.redirect('/cursos');
});

app.get('/cursos/:id/abrir', (req, res) => {
    try {
        let curso = cursos.buscarCurso(parseInt(req.params.id));
        if (curso === null) {
            throw('¡Curso no encontrado!');
        }

        curso.estado = 'disponible';
        flash.agregarMensaje('Curso abierto exitosamente');
    } catch (error) {
        flash.agregarError(error);
    }
    res.redirect('/cursos');
});

app.get('/cursos/:id/inscripciones/:cedula/quitar', (req, res) => {
    try {
        alumnos.quitarInscripcion(req.params.cedula, parseInt(req.params.id));
        flash.agregarMensaje('La inscripción ha sido borrada exitosamente');
    } catch (error) {
        flash.agregarError(error);
    }

    res.redirect('/cursos/' + req.params.id + '/inscripciones');
});

app.get('/inscripciones', (req, res) => {
    let listaCursos = cursos.obtenerListaCursos();
    let { mensajes, errores} = flash.obtenerFlash();
    res.render('inscripciones', {'cursos' : listaCursos, mensajes : mensajes, errores: errores});
});

app.post('/inscribir', (req, res) => {
    try {
        let alumno = {
            nombre: req.body.nombre,
            cedula: req.body.cedula,
            telefono: req.body.telefono,
            correo: req.body.correo
        };

        iniciarRegistro(alumno, parseInt(req.body.id_curso), false);
        flash.agregarMensaje('¡Te has inscrito exitosamente a este curso!');
    } catch (error) {
        flash.agregarError(error);
    }

    res.redirect('/cursos/disponibles/' + req.body.id_curso);
});

app.listen(3000, () => {
    console.log('Servidor escuchando en el puerto 3000');
});