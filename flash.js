var mensajes = [];
var errores = [];

let agregarMensaje = mensaje => {
    mensajes.push({mensaje : mensaje});
}

let agregarError = error => {
    errores.push({error : error});
}

let obtenerFlash = () => {
    let _mensajes = mensajes.slice(0);
    let _errores = errores.slice(0);

    mensajes = [];
    errores = [];

    return {mensajes : _mensajes, errores : _errores};
}

module.exports = {
    agregarError,
    agregarMensaje,
    obtenerFlash
};