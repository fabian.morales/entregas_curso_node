let listaCursos = require('./datos/cursos.json');
const fs = require('fs');

let cursoToString = curso => {
    let { id, nombre, intensidad, valor } = curso;
    return 'Curso ID# ' + id + ': ' 
        + nombre + ', tiene una intensidad horaria de ' + intensidad 
        + ' y un costo de $ ' + valor;
}

let escribirArchivo = (callback) => {
    fs.writeFile('./datos/cursos.json', JSON.stringify(listaCursos), (err) => {
        if (err){
            throw (err);
        }
        else{
            callback();
        }
    });
}

let agregarCurso = (curso, callback) => {
    if (listaCursos == null) {
        listaCursos = [];
    }

    if (curso.nombre === '') {
        throw('Debe ingresar el nombre del curso');
    }

    if (curso.descripcion === '') {
        throw('Debe ingresar la descripción del curso');
    }

    if (curso.valor === '') {
        throw('Debe ingresar el valor del curso');
    }

    if (buscarCurso(curso.id) !== null){
        throw('¡Ya existe un curso con este id!');
    }

    listaCursos.push(curso);
    escribirArchivo(() => {
        if (callback !== null && typeof callback !== 'undefined') {
            callback();
        }
    });
}

let mostrarCurso = curso => {
    console.log(cursoToString(curso));
}

let buscarCurso = id => {
    let curso = listaCursos.find(e => {
        return e.id === id;
    });

    if (typeof curso === 'undefined') {
        curso = null;
    }

    return curso;
}

let actualizarCurso = (curso, callback) => {
    if (curso.id === '') {
        throw('Debe ingresar el identificador del curso');
    }

    if (curso.nombre === '') {
        throw('Debe ingresar el nombre del curso');
    }

    if (curso.descripcion === '') {
        throw('Debe ingresar la descripción del curso');
    }

    if (curso.valor === '') {
        throw('Debe ingresar el valor del curso');
    }

    let cursoTmp = buscarCurso(curso.id);
    if (cursoTmp === null) {
        throw('¡Curso no encontrado!');
    }

    cursoTmp.nombre = curso.nombre;
    cursoTmp.intensidad = curso.intensidad;
    cursoTmp.descripcion = curso.descripcion;
    cursoTmp.valor = curso.valor;
    cursoTmp.modalidad = curso.modalidad;
    cursoTmp.estado = curso.estado || 'disponible';

    let index = listaCursos.indexOf(cursoTmp);
    listaCursos[index] = cursoTmp;

    escribirArchivo(() => {
        if (callback !== null && typeof callback !== 'undefined') {
            callback();
        }
    });
}

let pos = 0;
let mostrarListaCursosRetardo = () => {
    if (pos <= listaCursos.length - 1) {
        setTimeout(() => {
            mostrarCurso(listaCursos[pos]);
            pos += 1;
            mostrarListaCursosRetardo();
        }, pos === 0 ? 0 : 2000);
    }
}

let mostrarListaCursos = () => {
    listaCursos.forEach((curso) => {
        mostrarCurso(curso);
    });
}

let obtenerListaCursos = (soloDisponibles) => {
    if (typeof soloDisponibles !== 'undefined' && soloDisponibles){
        let _lista = listaCursos.filter(e => {
            return (e.estado === 'disponible');
        });

        return _lista;
    }
    return listaCursos;
}

module.exports = {
    cursoToString,
    mostrarCurso,
    buscarCurso,
    mostrarListaCursos,
    mostrarListaCursosRetardo,
    agregarCurso,
    actualizarCurso,
    obtenerListaCursos
};
