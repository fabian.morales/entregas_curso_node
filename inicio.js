const cursos = require('./curso');
const alumnos = require('./alumno');
const { iniciarRegistro } = require('./registro');
const opcComando = require('./opcionesComandos');

const argv = require('yargs')
    .command('inscribir', 'Realiza una inscripción a un curso especificado', opcComando.inscribirCurso)
    .command('agregarcurso', 'Almacena un curso', opcComando.opcionesCurso)
    .command('actualizarcurso', 'Actualiza un curso', opcComando.opcionesCurso)
    .command('agregaralumno', 'Almacena un alumno', opcComando.opcionesAlumno)
    .command('actualizaralumno', 'Actualiza un alumno', opcComando.opcionesAlumno)
    .command('listarlumnos', 'Muestra todos los alumnos registrados', {})
    .command('asignarnota', 'Asigna la nota de un alumno para un curso que tenga inscrito', opcComando.opcionesAsignarNota)
    .command('mostrarinscripciones', 'Muestra los cursos inscritos por el alumno con sus respectivas notas', opcComando.mostrarInscripciones)
    .command('mostrarpromedios', 'Muestra los alumnos con promedios de notas altos', {})
    .argv;

var textoInscripcion = "";

try {
    switch (argv._[0]) {
        case 'inscribir':
            textoInscripcion = iniciarRegistro(argv.n, argv.c, argv.i, argv.a === 1);
            const express = require('express');
            var app = express();
            app.get('/', (req, res) => {
                res.send(textoInscripcion);
            });
            app.listen(3000, () => {
                console.log('Servidor escuchando en el puerto 3000');
            });
            break;
    
        case 'agregarcurso':
            let cursoNuevo = {
                id: argv.id,
                nombre: argv.n,
                descripcion: argv.d,
                intensidad: argv.t,
                valor: argv.v
            };
            cursos.agregarCurso(cursoNuevo, () => {
                console.log('Curso guardado exitosamente \r\n' + 'La lista de cursos es la siguiente: \r\n');
                mostrarListaCursos();
            });
            break;
    
        case 'actualizarcurso':
            let curso = {
                id: argv.id,
                nombre: argv.n,
                descripcion: argv.d,
                intensidad: argv.t,
                valor: argv.v
            };
            cursos.actualizarCurso(curso, () => {
                console.log('Curso actualizado exitosamente');
            });
            break;
    
        case 'agregaralumno':
            let alumnoNuevo = {
                nombre: argv.n,
                cedula: argv.c
            };
            alumnos.agregarAlumno(alumnoNuevo, () => {
                console.log('Alumno registrado exitosamente \r\n' + 'Listado de alumnos registrados: \r\n');
                mostrarListaAlumnos();
            });
            break;
    
        case 'actualizaralumno':
            let alumno = {
                nombre: argv.n,
                cedula: argv.c
            };
            alumnos.actualizarAlumno(alumno, () => {
                console.log('Alumno actualizado exitosamente');
            });
            break;

        case 'listaralumnos':
            alumnos.mostrarListaAlumnos();
            break;

        case 'asignarnota':
            alumnos.asignarNota(argv.c, argv.i, argv.n, () => {
                console.log('Nota asignada exitosamente');
            });
            break;

        case 'mostrarinscripciones':
            alumnos.mostrarInscripciones(argv.c);
            break;

        case 'mostrarpromedios':
            alumnos.mostrarPromediosAltos();
            break;
    
        default:
            if(typeof argv.i !== 'undefined' && argv.i !== ''){
                let curso = cursos.buscarCurso(argv.i);
                if (curso !== null) {
                    cursos.mostrarCurso(curso);
                }
                else{
                    console.log('¡Curso no encontrado! \n' + 'Recuerde que nuestra oferta de cursos es la siguiente: \n ');
                    cursos.mostrarListaCursosRetardo();
                }
            }
            else{
                cursos.mostrarListaCursosRetardo();
            }
            break;
    }
} 
catch (error) {
    console.log(error);
}
