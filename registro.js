const { cursoToString, mostrarCurso, buscarCurso, mostrarListaCursos } = require('./curso');
const alumnos = require('./alumno');
const fs = require('fs');

let iniciarRegistro = (alumno, idCurso, guardar, callback) => {
    let ret = "";

    if (alumno.nombre === '') {
        throw('Debe ingresar su nombre');
    }

    if (alumno.cedula === '') {
        throw('Debe ingresar su cédula');
    }

    if (alumno.telefono === '') {
        throw('Debe ingresar su teléfono');
    }

    if (alumno.correo === '') {
        throw('Debe ingresar su correo');
    }

    let curso = buscarCurso(idCurso);
    if (curso !== null) {
        if (curso.estado !== 'disponible') {
            throw('El curso se encuentra cerrado');
        }

        if (alumnos.buscarAlumno(alumno.cedula) == null) {
            alumnos.agregarAlumno(alumno);
        }
        else {
            alumnos.actualizarAlumno(alumno);
        }

        alumnos.agregarInscripcion(alumno.cedula, curso);
        /*ret = realizarInscripcion(nombre, cedula, curso, guardar);
        mostrarCurso(curso);*/
    }
    else{
        /*ret = '¡Curso no encontrado!';
        console.log(ret + '\n' + 'Recuerde que nuestra oferta de cursos es la siguiente: \n ');
        mostrarListaCursos();*/
        throw('¡Curso no encontrado!');
    }

    return ret;
}

let realizarInscripcion = (nombre, cedula, curso, guardar) => {
    let texto = 'Nombre del interesado: ' + nombre + '\r\n' 
        + 'Cédula del interesado: ' + cedula + '\r\n'
        + 'Datos del curso matriculado: \r\n' + cursoToString(curso);

    if (guardar) {
        fs.writeFile('inscripcion_' + cedula + '_' + curso.id + '.txt', texto, (err) => {
            if (err){
                throw (err);
            }
            else{
                console.log('Inscripción realizada y almacenada exitosamente');
            }
        });
    }
    else{
        console.log('Inscripción realizada exitosamente');
    }

    return texto.replace(/\r\n/g, '<br />');;
}

module.exports = {
    buscarCurso,
    iniciarRegistro,
    realizarInscripcion
};
