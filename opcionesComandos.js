const inscribirCurso = {
    nombre: {
        alias: 'n',
        demand: true
    },
    cedula: {
        alias: 'c',
        demand: true
    },
    id: {
        alias: 'i',
        demand: true
    },
    archivar: {
        alias: 'a',
        demand: false,
        default: 0
    }
};

const opcionesCurso = {
    id: {
        alias: 'i',
        demand: true
    },
    nombre: {
        alias: 'n',
        demand: true
    },
    descripcion: {
        alias: 'd',
        demand: true
    },
    intensidad: {
        alias: 't',
        demand: true
    },
    valor: {
        alias: 'v',
        demand: true
    },
};

const opcionesAlumno = {
    cedula: {
        alias: 'c',
        demand: true
    },
    nombre: {
        alias: 'n',
        demand: true
    },
};

const opcionesAsignarNota = {
    cedula: {
        alias: 'c',
        demand: true
    },
    idCurso: {
        alias: 'i',
        demand: true
    },
    nota: {
        alias: 'n',
        demand: true
    },
};

const mostrarInscripciones = {
    cedula: {
        alias: 'c',
        demand: true
    }
}

module.exports = {
    inscribirCurso,
    agregarCurso,
    actualizarCurso,
    opcionesAlumno,
    opcionesAsignarNota
}