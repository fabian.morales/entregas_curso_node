# Curso NodeJS

Autor: Fabián Morales
Correo: fabian.morales@outlook.com

Trabajo de aprendizaje de NodeJS.
Curso de NodeJS. Tecnológico de Antioquia.

## Pre-requisitos

Esta aplicación requiere los paquete de yargs y de express. Debe ejecutar este comando para una instalación global de dependencias:

> npm install

## Versión WEB

La versión web es la utilizada para cumplir con los criterios de aceptación de la entrega 2. Debe ejecutarse
con el comando:

> node index

Luego abra la siguiente url en su navegador:

> http://localhost:3000

Se le presentará la página inicial del sitio. Esta cuenta con un menú que le dará accesoa las opciones del sistema:

- Cursos disponibles: muestra en una tabla el listado de los cursos con estado "Disponible", cada curso cuenta con un link en su nombre para ver el detalle del curso. En la página de este detalle, se encuentra el formulario para que un aspirante se inscriba en el curso que está visualizando.

- Gestión de cursos: en esta sección el coordinador podrá visualizar todos los cursos registrados, dándole la opción de crear cursos nuevos, editar los existentes, cambiar su estado (disponible a cerrado y viceversa)
y visualizar los aspirantes que se han inscrito en el curso. En la vista de aspirantes inscritos, en cada registro se da la opción de cancelar la inscripción.

- Inscripciones: permite al coordinador cancelar una inscripción seleccionando el curso y digitando el cédula del estudiante. Tiene la misma funcionalidad que la vista de aspirantes inscritos que se provee en la opción de Gestión de Cursos.

# Versión Consola

Permite realizar todas las opciones que se proveen en la interfaz web, pero desde comandos de la consola.

## NOTA IMPORTANTE:

Esta versión no está funcionando temporalmente, debido a los cambios realizados para la interfaz web. En próximos releases se completará esta versión de consola de la aplicación

## Historia de usuario 1:
Como interesado necesito obtener la información de cursos ofertados por educación continua
para así tener diferentes opciones para elegir posteriormente un curso al cual matricularme.

### Comando
> node inicio

Se mostrará todos los cursos mostrando un retraso de 2 segundos entre curso y curso

## Historia de usuario 2:
Como interesado necesito seleccionar un curso para ingresar mis datos y quedar como
prematriculado.

### Comando
> node inicio inscribir -n=[tu_nombre] -c=[tu_cedula] -i=[id_curso] -a=[generar_archivo]

El nombre, la cédula y el id del curso son obligatorios. El parámetro para 
generar el archivo (-a) es opcional.

Si el curso no es encontrado, se mostrará un mensaje de alerta. De lo contrario, se
mostrará la información del curso y se realizará la inscripción. La información de la
incripción realizada podrá verse en el navegador entradando a esta dirección:

> http://localhost:3000

Adicional a esto, si se desea generar también el archivo debe pasarse un valor de "1"
al parámetro "-1"

> node inicio inscribir -n=[tu_nombre] -c=[tu_cedula] -i=[id_curso] -a=1

Y se generará el archivo de la inscripción, el cual tendrá la siguiente estructura para su nombre:

> inscripcion_[tu_cedula]_[id_curso].txt

### Comando para buscar un curso específico
> node inicio -i=[id_curso]

### Visualizar el promedio de los estudiantes

Antes de usar este comando, debe tenerse en cuenta lo siguiente:
1. Debe haber estudiantes inscritos a cursos
2. Se debe haber asignado una nota a cada estudiante para cada curso que tenga matriculado}
3. Solo se mostrarán los promedios mayores que 4.0

Para asignar una nota a un alumno debe usarse este comando:
> node inicio asignarnota -i=[id_curso] -c=[cedula_alumno] -n=[nota]

Una vez que haya alumnos inscritos a cursos y tengan notas asignadas, puede verse el promedio
de notas con este comando

> node inicio mostrarpromedio

### Otros comandos

Mostrar los cursos inscritos por un alumno

> node inicio mostrarinscripciones -c[cedula_alumno]

Mostrar todos los alumnos

> node inicio listaralumnos

## Releases

### v1.0. Primera entrega

Implementación de las historias de usuario 1 y 2

### v1.1. Entrega opcional [bonus]

Se puede visualizar la inscripción realizada en el navegador. La impresión del archivo de la inscripción
es opcional.

### v1.2. Entrega opcional [bonus]

Se puede asignar notas a los alumnos para los cursos que tengan asignados y ver los promedios altos.

### v1.3. Segunda entrega

Implementación de las historias de usuario 1 a 5 para la versión web del sitio, cumpliendo con los criterios para los diferentes perfiles definidos para la aplicación.